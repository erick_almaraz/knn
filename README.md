i) Identification of astronomical objects in the SDSS DR14 catalog
In this exercise we classify astronomical objects into stars, galaxies and quasars in the SDSS DR14 survey.

ii) Identification of pulsar candidate signals
In this exercise we predict a pulsar star from a series of measurements collected by the HTRU survey.

The exercises will be updated as I learn something new. Suggestions and comments are welcome!